import function.FunctionUtil;
import function.FunctionUtilParallel;
import model.category.Category;
import model.category.SubCategory;
import model.item.Item;
import model.item.ItemRentLine;
import model.item.RentType;
import model.qna.Answer;
import model.qna.Question;
import model.rent_request.Rating;
import model.rent_request.RentRequest;
import model.user.Rentee;
import model.user.Renter;
import model.user.Role;
import model.user.User;
import model.utils.Status;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionUtilParallelTest {
    private List<Category> categories;
    private List<SubCategory> subCategories;

    private List<Item> items;
    private List<ItemRentLine> itemRentLines;

    private List<Answer> answers;
    private List<Question> questions;

    private List<Rating> ratings;
    private List<RentRequest> rentRequests;

    private List<User> users;
    private List<Role> roles;

    private User admin, rentee1, rentee2, renter1, renter2;

    private Category electronics, home, clothes;
    private SubCategory computer, mobile, chair, sofa, jean, tshirt;

    private Item item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12;
    private ItemRentLine itemRentLine1, itemRentLine2, itemRentLine3, itemRentLine4, itemRentLine5, itemRentLine6,
            itemRentLine7, itemRentLine8, itemRentLine9, itemRentLine10, itemRentLine11, itemRentLine12;

    private Question question1, question2, question3, question4, question5, question6, question7, question8, question9, question10, question11;
    private Answer answer1, answer2, answer3, answer4, answer5, answer6, answer7, answer8, answer9, answer10, answer11;

    private RentRequest rentRequest1, rentRequest2, rentRequest3, rentRequest4, rentRequest5, rentRequest6, rentRequest7;
    private Rating rating1, rating2, rating3, rating4, rating5, rating6;

    @Before
    public void init() {
        // admin
        admin = TestFactory.createRentee("admin", "admin@admin.com");
        // rentee
        rentee1 = TestFactory.createRentee("rentee1", "rentee1@rentee1.com");
        rentee2 = TestFactory.createRentee("rentee2", "rentee2@rentee2.com");
        // renter
        renter1 = TestFactory.createRenter("renter1", "renter1@renter1.com");
        renter2 = TestFactory.createRenter("renter2", "renter2@renter2.com");

        // category
        electronics = TestFactory.createCategory("Electronics", "Electronics Desc", admin);
        home = TestFactory.createCategory("Home", "Home Desc", admin);
        clothes = TestFactory.createCategory("Clothes", "Clothes Desc", admin);

        // sub-category
        computer = TestFactory.createSubCategory("Computer", "Computer Desc", admin, electronics);
        mobile = TestFactory.createSubCategory("Mobile", "Mobile Desc", admin, electronics);

        chair = TestFactory.createSubCategory("Chair", "Chair Desc", admin, home);
        sofa = TestFactory.createSubCategory("Sofa", "Sofa Desc", admin, home);

        jean = TestFactory.createSubCategory("Jean", "Jean Desc", admin, clothes);
        tshirt = TestFactory.createSubCategory("T-Shirt", "T-Shirt Desc", admin, clothes);

        // item
        item1 = TestFactory.createItem("computer-1", "computer-1", renter1, computer);
        item2 = TestFactory.createItem("computer-2", "computer-2", renter1, computer);

        item3 = TestFactory.createItem("mobile-1", "computer-1", renter1, mobile);
        item4 = TestFactory.createItem("mobile-2", "computer-2", renter2, mobile);

        item5 = TestFactory.createItem("chair-1", "computer-1", renter1, chair);
        item6 = TestFactory.createItem("chair-2", "computer-2", renter1, chair);

        item7 = TestFactory.createItem("sofa-1", "sofa-1", renter1, sofa);
        item8 = TestFactory.createItem("sofa-2", "sofa-2", renter2, sofa);

        item9 = TestFactory.createItem("jean-1", "jean-1", renter1, jean);
        item10 = TestFactory.createItem("jean-2", "jean-1", renter1, jean);

        item11 = TestFactory.createItem("tshirt-1", "tshirt-1", renter1, tshirt);
        item12 = TestFactory.createItem("tshirt-2", "tshirt-2", renter2, tshirt);

        //item-rent-line
        itemRentLine1 = TestFactory.createItemRentLine(20.0, 2020, 10, RentType.DAILY, item1, renter1);
        itemRentLine2 = TestFactory.createItemRentLine(25.0, 2020, 15, RentType.DAILY, item2, renter1);

        itemRentLine3 = TestFactory.createItemRentLine(5.0, 2020, 15, RentType.DAILY, item3, renter1);
        itemRentLine4 = TestFactory.createItemRentLine(8.0, 2020, 20, RentType.DAILY, item4, renter2);

        itemRentLine5 = TestFactory.createItemRentLine(3.0, 2020, 30, RentType.DAILY, item5, renter1);
        itemRentLine6 = TestFactory.createItemRentLine(4.0, 2020, 25, RentType.DAILY, item6, renter1);

        itemRentLine7 = TestFactory.createItemRentLine(5.0, 2020, 20, RentType.DAILY, item7, renter1);
        itemRentLine8 = TestFactory.createItemRentLine(4.0, 2020, 25, RentType.DAILY, item8, renter2);

        itemRentLine9 = TestFactory.createItemRentLine(8.0, 2020, 30, RentType.DAILY, item9, renter1);
        itemRentLine10 = TestFactory.createItemRentLine(7.0, 2020, 30, RentType.DAILY, item10, renter1);

        itemRentLine11 = TestFactory.createItemRentLine(2.0, 2020, 5, RentType.DAILY, item11, renter1);
        itemRentLine12 = TestFactory.createItemRentLine(3.0, 2020, 10, RentType.DAILY, item12, renter2);

        //rent-request
        rentRequest1 = TestFactory.createRentRequest(20.0, 2020, 10, Status.Accepted, itemRentLine1, rentee1);
        rentRequest2 = TestFactory.createRentRequest(20.0, 2020, 10, Status.Pending, itemRentLine1, rentee2);
        rentRequest3 = TestFactory.createRentRequest(5.0, 2020, 10, Status.Pending, itemRentLine3, rentee1);
        rentRequest4 = TestFactory.createRentRequest(5.0, 2020, 10, Status.Pending, itemRentLine3, rentee2);
        rentRequest5 = TestFactory.createRentRequest(3.0, 2020, 5, Status.Accepted, itemRentLine5, rentee1);
        rentRequest6 = TestFactory.createRentRequest(3.0, 2020, 5, Status.Accepted, itemRentLine5, rentee2);
//        rentRequest7 = TestFactory.createRentRequest(20.0, 2020, 10, Status.Rejected, itemRentLine1, rentee2);

        //raiting
        rating1 = TestFactory.createRating(5, 4, 4, rentRequest1, rentee1);
        rating2 = TestFactory.createRating(4, 4, 3, rentRequest2, rentee2);
        rating3 = TestFactory.createRating(3, 3, 3, rentRequest3, rentee1);
        rating4 = TestFactory.createRating(5, 4, 4, rentRequest4, rentee2);
        rating5 = TestFactory.createRating(5, 4, 5, rentRequest5, rentee1);
        rating6 = TestFactory.createRating(5, 5, 5, rentRequest6, rentee2);

        //question
        question1 = TestFactory.createQuestion("qs1", itemRentLine1, rentee1);
        question2 = TestFactory.createQuestion("qs2", itemRentLine1, rentee1);
        question3 = TestFactory.createQuestion("qs3", itemRentLine1, rentee1);
        question4 = TestFactory.createQuestion("qs4", itemRentLine2, rentee2);
        question5 = TestFactory.createQuestion("qs5", itemRentLine2, rentee2);
        question6 = TestFactory.createQuestion("qs6", itemRentLine2, rentee2);
        question7 = TestFactory.createQuestion("qs7", itemRentLine3, rentee1);
        question8 = TestFactory.createQuestion("qs8", itemRentLine3, rentee2);
        question9 = TestFactory.createQuestion("qs9", itemRentLine4, rentee2);
        question10 = TestFactory.createQuestion("qs10", itemRentLine5, rentee1);
        question11 = TestFactory.createQuestion("qs11", itemRentLine6, rentee2);
        question6 = TestFactory.createQuestion("qs12", itemRentLine1, rentee2);


        //answer
        answer1 = TestFactory.createAnswer("ans1", question1, rentee1);
        answer2 = TestFactory.createAnswer("ans2", question1, rentee1);
        answer3 = TestFactory.createAnswer("ans3", question1, rentee1);
        answer4 = TestFactory.createAnswer("ans4", question2, rentee2);
        answer5 = TestFactory.createAnswer("ans5", question2, rentee2);
        answer6 = TestFactory.createAnswer("ans6", question2, rentee2);
        answer7 = TestFactory.createAnswer("ans7", question3, rentee1);
        answer8 = TestFactory.createAnswer("ans8", question3, rentee2);
        answer9 = TestFactory.createAnswer("ans9", question4, rentee1);
        answer10 = TestFactory.createAnswer("ans10", question7, rentee1);
        answer11 = TestFactory.createAnswer("ans11", question9, rentee1);

        subCategories = Stream.of(computer, mobile, chair, sofa, jean, tshirt).collect(Collectors.toList());
        categories = Stream.of(electronics, home, clothes).collect(Collectors.toList());

        items = Stream.of(item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12)
                .collect(Collectors.toList());
        itemRentLines = Stream.of(
                itemRentLine1, itemRentLine2, itemRentLine3, itemRentLine4, itemRentLine5, itemRentLine6,
                itemRentLine7, itemRentLine8, itemRentLine9, itemRentLine10, itemRentLine11, itemRentLine12
        ).collect(Collectors.toList());

        answers = Stream.of(answer1, answer2, answer3, answer4, answer5, answer6, answer7, answer8,
                answer9, answer10, answer11).collect(Collectors.toList());
        questions = Stream.of(
                question1, question2, question3, question4, question5, question6, question7, question8, question9, question10, question11
        ).collect(Collectors.toList());

        ratings = Stream.of(
                rating1, rating2, rating3, rating4, rating5, rating6
        ).collect(Collectors.toList());

        users = Stream.of(
                admin, rentee1, rentee2, renter1, renter2
        ).collect(Collectors.toList());
    }

    //----------- Qsn 1 Test -------------
    @Test
    public void topKRatedItems_test() {
        int k = 5;
        List<Item> output =  FunctionUtilParallel.topKRatedItems.apply(items, k);
        Assert.assertEquals(k, output.size());
        Assert.assertTrue(output.get(0).getName().equalsIgnoreCase("chair-1"));
        Assert.assertTrue(output.get(1).getName().equalsIgnoreCase("computer-1"));
    }

    @Test
    public void calculateRating_Test(){
        Long sum1 = FunctionUtilParallel.calculateRating.apply(item1);
        Assert.assertEquals(9, sum1.longValue());

        Long sum2 = FunctionUtilParallel.calculateRating.apply(item3);
        Assert.assertEquals(8, sum2.longValue());

        Long sum3 = FunctionUtilParallel.calculateRating.apply(item7);
        Assert.assertEquals(0, sum3.longValue());
    }
    //----------- End of Qsn 1 Test -------------

    //----------- Qsn 7 Test -------------
    @Test
    public void topKRentersMadeMostMoneyInAYear_test(){
        List<User> output1 = FunctionUtilParallel.topKRentersMadeMostMoneyInAYear.apply(items, 5, 2020);
        List<String> userNames1 = output1.stream().map(User::getUsername).collect(Collectors.toList());
        Assert.assertEquals(1, output1.size());
        Assert.assertThat(userNames1, Matchers.contains("renter1"));

        List<User> output2 = FunctionUtilParallel.topKRentersMadeMostMoneyInAYear.apply(items, 5, 2011);
        Assert.assertEquals(0, output2.size());
    }

    @Test
    public void calculateEarningsOfAnItem_Test(){
        Double totalEarnings1 = FunctionUtilParallel.calculateEarningsOfAnItem.apply(item1, 2020);
        Assert.assertEquals(400.0, totalEarnings1, 0.0);

        Double totalEarnings2 = FunctionUtilParallel.calculateEarningsOfAnItem.apply(item1, 2021);
        Assert.assertEquals(0.0, totalEarnings2, 0.0);
    }
    //----------- End of Qsn 7 Test -------------

    //----------- Qsn 13 Test -------------
    @Test
    public void topKItemsHavingMostRentRequestRejection_Test(){
        List<Item> output = FunctionUtilParallel.topKItemsHavingMostRentRequestRejection.apply(items, 5);
        Assert.assertEquals(0, output.size());
    }

    @Test
    public void countTheRejectionStatusOfRentRequestOfAnItem_Test(){
        Long count1 = FunctionUtilParallel.countTheRejectionStatusOfRentRequestOfAnItem.apply(item1);
        Assert.assertEquals(0, count1.longValue());
    }
    //----------- End of Qsn 13 Test -------------

    @Test
    public void test_topKItemWhoMadeMostMoney() {

        List<Item> items = new ArrayList<>();
        items.add(item3);
        items.add(item5);
        items.add(item1);

        var result = FunctionUtilParallel.topKItemWhoMadeMostMoney.apply(items, 2020,1);
//        System.out.println(result.get(0).getName());
        List<Item> res = new ArrayList<>();
        res.add(item1);

        Assert.assertEquals(res, result);

    }

    @Test
    public void test_calculateSumRentPrice() {

        var result = FunctionUtilParallel.calculateSumRentPrice.apply(item1, 2020);
        Double res = 20.0;
        Assert.assertEquals(res, result);

    }


    @Test
    public void test_countTotalRentRequest() {

        long result = FunctionUtilParallel.countTotalRentRequest.apply(item1, 2020);
        Assert.assertEquals(2, result);

    }


    //Query #3
    @Test
    public void topKRenteesForYearTest() {
        List<String> rentees = FunctionUtilParallel.topKRenteesForYear.apply(users, 2020, 1);

        Assert.assertEquals("rentee1@rentee1.com", rentees.get(0));
    }

    //Query #9
    @Test
    public void topKItemsHaveMostQuestionsTest() {
        List<String> items = FunctionUtilParallel.topKItemsHaveMostQuestions.apply(users, 2020, 2);

        List<String> expectedItems = Stream.of("computer-1", "computer-2").collect(Collectors.toList());

        Assert.assertEquals(expectedItems, items);
    }

    //Query #15
    @Test
    public void topKItemsHavePendingRequestsTest() {
        List<String> items = FunctionUtilParallel.topKItemsHavePendingRequests.apply(users, 2020, 2);

        List<String> expectedItems = Stream.of("mobile-1", "computer-1").collect(Collectors.toList());

        Assert.assertEquals(expectedItems, items);
    }

    @Test
    public void test_topKItemsHaveMostRentRequest() {

        List<Item> items = new ArrayList<>();
        items.add(item2);
        items.add(item1);

        var result = FunctionUtilParallel.topKItemsHaveMostRentRequest.apply(items, 2020, 1);

        List<Item> res = new ArrayList<>();
        res.add(item1);
        Assert.assertEquals(res, result);

    }

    //# Quest : #16
    @Test
    public void test_topKItemsNotRecievedAnyRequest() {

        var result = FunctionUtilParallel.topKItemsNotRecievedAnyRequest.apply(items, 2020, 12);
        Assert.assertTrue(result.size() == 9);

    }

    //# Quest : #10
    @Test
    public void test_topKRentersAskedMostQuestions() {

        var result = FunctionUtilParallel.topKRentersAskedMostQuestions.apply(users, 2020, 1);
        Assert.assertTrue(result.get(0).equals("renter1@renter1.com"));

    }

    //# Quest : #4
    @Test
    public void test_topKSubCategoriesHaveMostRentedItems() {

        var result = FunctionUtilParallel.topKSubCategoriesHaveMostRentedItems.apply(items, 2020, 1);
        Assert.assertTrue(result.get(0).equals("Chair"));

    }


    @Test
    public void test_getTopKItemsWithUnAnsweredQuestions() {
        var result = FunctionUtilParallel.getTopKItemsWithUnAnsweredQuestions.apply(items, 1);
        List<String> names = result.stream().map(x -> x.getName()).collect(Collectors.toList());

        Assert.assertThat(names, Matchers.contains("computer-2"));
    }

    @Test
    public void test5_getTopKRentedItems() {
        var result = FunctionUtilParallel.getTopKRentedItems.apply(items, 1);
        List<String> names = result.stream().map(x -> x.getName()).collect(Collectors.toList());

        Assert.assertThat(names, Matchers.contains("chair-1"));
    }


    @Test
    public void test11_getTopKusersAnsweredMostQuestions() {
        var result = FunctionUtilParallel.getTopKusersAnsweredMostQuestions.apply(items, 2020, 1);
        List<String> names = result.stream().map(x -> x.getUsername()).collect(Collectors.toList());

        Assert.assertThat(names,Matchers.contains("rentee1"));
    }


    @Test
    public void test_topKRentersHaveRegisteredItem(){
        List<User> users = new ArrayList<>();
        users.add(renter2);
        users.add(renter1);

        var result = FunctionUtilParallel.topKRentersHaveRegisteredItem.apply(users, 2020, 1);
        Assert.assertEquals("renter1@renter1.com",result);

    }

    //2,8,14
    @Test
    public void topKRatedRenter_test() {
        int k = 1;
        List<Renter> output =  FunctionUtilParallel.topKRatedRenter.apply(users, 2020, k);
        Assert.assertEquals(k, output.size());
        Assert.assertTrue(output.get(0).getUser().getUsername().equalsIgnoreCase("renter1"));
    }


    @Test
    public void topKRenteesSpentMostMoneyInAYear_test(){
        List<Rentee> output1 = FunctionUtilParallel.topKRenteesSpentMostMoneyInAYear.apply(users, 5, 2020);
        List<String> userNames1 = output1.stream().map(r -> r.getUser().getUsername()).collect(Collectors.toList());
        Assert.assertEquals(3, output1.size());
        Assert.assertTrue(userNames1.contains("rentee1"));

    }


    @Test
    public void topKItemsHavingMostRentRequestAcceptence_Test(){
        List<Item> output = FunctionUtilParallel.topKItemsHavingMostRentRequestAcceptance.apply(items, 5);
        Assert.assertEquals(5, output.size());
    }

    @Test
    public void countTheAcceptenceStatusOfRentRequestOfAnItem_Test(){
        Long count1 = FunctionUtilParallel.countTheAcceptanceStatusOfRentRequestOfAnItem.apply(item1);
        Assert.assertEquals(1, count1.longValue());
    }
    // end of 2,8,14
}
