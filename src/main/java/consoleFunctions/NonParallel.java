package consoleFunctions;

import factory.Factory;
import function.FunctionUtil;
import model.category.Category;
import model.category.SubCategory;
import model.item.Item;
import model.item.ItemRentLine;
import model.item.RentType;
import model.qna.Answer;
import model.qna.Question;
import model.rent_request.Rating;
import model.rent_request.RentRequest;
import model.user.Rentee;
import model.user.Renter;
import model.user.User;
import model.utils.Status;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NonParallel {
    private static Scanner scanner;

    private static List<Item> items;
    private static List<User> users;

    private static User admin, rentee1, rentee2, renter1, renter2;

    private static Category electronics, home, clothes;
    private static SubCategory computer, mobile, chair, sofa, jean, tshirt;

    private static Item item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12;
    private static ItemRentLine itemRentLine1, itemRentLine2, itemRentLine3, itemRentLine4, itemRentLine5, itemRentLine6,
            itemRentLine7, itemRentLine8, itemRentLine9, itemRentLine10, itemRentLine11, itemRentLine12;

    private static Question question1, question2, question3, question4, question5, question6, question7, question8, question9, question10, question11;
    private static Answer answer1, answer2, answer3, answer4, answer5, answer6, answer7, answer8, answer9, answer10, answer11;

    private static RentRequest rentRequest1, rentRequest2, rentRequest3, rentRequest4, rentRequest5, rentRequest6, rentRequest7;
    private static Rating rating1, rating2, rating3, rating4, rating5, rating6;

    public NonParallel() {
        init();
        scanner = new Scanner(System.in);

    }


    public static void getTopKRatedItems() {
        System.out.println("===== Calculating Top K Rated Items =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        List<Item> output = FunctionUtil.topKRatedItems.apply(items, k);

        if (output.isEmpty())
            printEmptyData();
        else {
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }

    public static void getTopKRentersMadeMostMoneyInAYear() {
        System.out.println("===== Calculating Top K Renters Who Made Most Money In A Year =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();
        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<User> output = FunctionUtil.topKRentersMadeMostMoneyInAYear.apply(items, k, year);

        if (output.isEmpty())
            printEmptyData();
        else {
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }

    public static void getTopKItemsHavingMostRentRequestRejection() {
        System.out.println("===== Calculating Top K Items Which Have Most Rent Request Rejection =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        List<Item> output = FunctionUtil.topKItemsHavingMostRentRequestRejection.apply(items, k);

        if (output.isEmpty())
            printEmptyData();
        else {
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }

    public static void getTopKRenteesForYear() {
        System.out.println("===== Calculating top K rentees which rented most itemes sor a Sspecific year  =====");
        System.out.print("Enter how many rentees you want to display : ");
        int k = scanner.nextInt();

        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<String> output = FunctionUtil.topKRenteesForYear.apply(users, year, k);

        if (output.isEmpty())
            printEmptyData();
        else {
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }

    public static void getTopKItemsHaveMostQuestions() {
        System.out.println("===== Calculating top K items which have most questions =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<String> output = FunctionUtil.topKItemsHaveMostQuestions.apply(users, year, k);

        if (output.isEmpty())
            printEmptyData();
        else {
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }

    public static void getTopKItemsHavePendingRequests() {
        System.out.println("===== Calculating top K items which have most pending rent-requests =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<String> output = FunctionUtil.topKItemsHavePendingRequests.apply(users, year, k);

        if (output.isEmpty())
            printEmptyData();
        else {
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }

    }

    public static void getTopKSubCategoriesHaveMostRentedItems(){
        System.out.println("===== Top K Sub Categories Which Have Most Rented Items =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<String> output = FunctionUtil.topKSubCategoriesHaveMostRentedItems.apply(items, year, k);

        if(output.isEmpty())
            printEmptyData();
        else{
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }

    public static void getTopKItemsNotRecievedAnyRequest(){
        System.out.println("===== Top K Sub Categories Which Have Most Rented Items =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<Item> output = FunctionUtil.topKItemsNotRecievedAnyRequest.apply(items, year, k);

        if(output.isEmpty())
            printEmptyData();
        else{
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }

    public static void getTopKRentersAskedMostQuestions(){
        System.out.println("===== Top K Sub Categories Which Have Most Rented Items =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<String> output = FunctionUtil.topKRentersAskedMostQuestions.apply(users, year, k);

        if(output.isEmpty())
            printEmptyData();
        else{
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }


    public static void getTopKRentedItems() {
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        var result = FunctionUtil.getTopKRentedItems.apply(items, k);
        List<String> names = result.stream().map(x -> x.getName()).collect(Collectors.toList());
        System.out.println(result);
    }

    public static void getTopKItemsWithUnAnsweredQuestions() {
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        var result = FunctionUtil.getTopKItemsWithUnAnsweredQuestions.apply(items, k);
        List<String> names = result.stream().map(x -> x.getName()).collect(Collectors.toList());
        System.out.println(names);
    }

    public static void getTopKusersAnsweredMostQuestions() {
        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        var result = FunctionUtil.getTopKusersAnsweredMostQuestions.apply(items, year, k);
        List<String> names = result.stream().map(x -> x.getUsername()).collect(Collectors.toList());
        System.out.println(names);
    }

    public static void getTopKRatedRenters(){
        System.out.println("===== Calculating Top K Rated Renters =====");
        System.out.print("Enter how many Renters do you want to be displayed : ");
        int k = scanner.nextInt();
        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<Renter> output = FunctionUtil.topKRatedRenter.apply(users, year, k);

        if(output.isEmpty())
            printEmptyData();
        else{
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i.getUser().getUsername()));
        }
    }

    public static void getTopKRenteesSpentMostMoneyInAYear(){
        System.out.println("===== Calculating Top K Rated Rentees  who spent most money in a specific year=====");
        System.out.print("Enter how many Rentees do you want to be displayed : ");
        int k = scanner.nextInt();
        System.out.print("Enter the year : ");
        int year = scanner.nextInt();

        List<Rentee> output = FunctionUtil.topKRenteesSpentMostMoneyInAYear.apply(users, year, k);

        if(output.isEmpty())
            printEmptyData();
        else{
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i.getUser().getUsername()));
        }
    }
    public static void getTopKItemsHavingMostRentRequestAcceptance(){
        System.out.println("===== Calculating Top K Items Which Have Most Rent Request Acceptance =====");
        System.out.print("Enter how many items you want to display : ");
        int k = scanner.nextInt();

        List<Item> output = FunctionUtil.topKItemsHavingMostRentRequestAcceptance.apply(items, k);

        if(output.isEmpty())
            printEmptyData();
        else{
            AtomicInteger ind = new AtomicInteger(0);
            output.forEach(i -> System.out.println(ind.incrementAndGet() + ". " + i));
        }
    }

    public static void printEmptyData() {
        System.out.println();
        System.out.println("NO DATA.. TRY AGAIN");
    }

    public static void init() {
        // admin
        admin = Factory.createRentee("admin", "admin@admin.com");
        // rentee
        rentee1 = Factory.createRentee("rentee1", "rentee1@rentee1.com");
        rentee2 = Factory.createRentee("rentee2", "rentee2@rentee2.com");
        // renter
        renter1 = Factory.createRenter("renter1", "renter1@renter1.com");
        renter2 = Factory.createRenter("renter2", "renter2@renter2.com");

        // category
        electronics = Factory.createCategory("Electronics", "Electronics Desc", admin);
        home = Factory.createCategory("Home", "Home Desc", admin);
        clothes = Factory.createCategory("Clothes", "Clothes Desc", admin);

        // sub-category
        computer = Factory.createSubCategory("Computer", "Computer Desc", admin, electronics);
        mobile = Factory.createSubCategory("Mobile", "Mobile Desc", admin, electronics);

        chair = Factory.createSubCategory("Chair", "Chair Desc", admin, home);
        sofa = Factory.createSubCategory("Sofa", "Sofa Desc", admin, home);

        jean = Factory.createSubCategory("Jean", "Jean Desc", admin, clothes);
        tshirt = Factory.createSubCategory("T-Shirt", "T-Shirt Desc", admin, clothes);

        // item
        item1 = Factory.createItem("computer-1", "computer-1", renter1, computer);
        item2 = Factory.createItem("computer-2", "computer-2", renter1, computer);

        item3 = Factory.createItem("mobile-1", "computer-1", renter1, mobile);
        item4 = Factory.createItem("mobile-2", "computer-2", renter2, mobile);

        item5 = Factory.createItem("chair-1", "computer-1", renter1, chair);
        item6 = Factory.createItem("chair-2", "computer-2", renter1, chair);

        item7 = Factory.createItem("sofa-1", "sofa-1", renter1, sofa);
        item8 = Factory.createItem("sofa-2", "sofa-2", renter2, sofa);

        item9 = Factory.createItem("jean-1", "jean-1", renter1, jean);
        item10 = Factory.createItem("jean-2", "jean-1", renter1, jean);

        item11 = Factory.createItem("tshirt-1", "tshirt-1", renter1, tshirt);
        item12 = Factory.createItem("tshirt-2", "tshirt-2", renter2, tshirt);

        //item-rent-line
        itemRentLine1 = Factory.createItemRentLine(20.0, 2020, 10, RentType.DAILY, item1, renter1);
        itemRentLine2 = Factory.createItemRentLine(25.0, 2020, 15, RentType.DAILY, item2, renter1);

        itemRentLine3 = Factory.createItemRentLine(5.0, 2020, 15, RentType.DAILY, item3, renter1);
        itemRentLine4 = Factory.createItemRentLine(8.0, 2020, 20, RentType.DAILY, item4, renter2);

        itemRentLine5 = Factory.createItemRentLine(3.0, 2020, 30, RentType.DAILY, item5, renter1);
        itemRentLine6 = Factory.createItemRentLine(4.0, 2020, 25, RentType.DAILY, item6, renter1);

        itemRentLine7 = Factory.createItemRentLine(5.0, 2020, 20, RentType.DAILY, item7, renter1);
        itemRentLine8 = Factory.createItemRentLine(4.0, 2020, 25, RentType.DAILY, item8, renter2);

        itemRentLine9 = Factory.createItemRentLine(8.0, 2020, 30, RentType.DAILY, item9, renter1);
        itemRentLine10 = Factory.createItemRentLine(7.0, 2020, 30, RentType.DAILY, item10, renter1);

        itemRentLine11 = Factory.createItemRentLine(2.0, 2020, 5, RentType.DAILY, item11, renter1);
        itemRentLine12 = Factory.createItemRentLine(3.0, 2020, 10, RentType.DAILY, item12, renter2);

        //rent-request
        rentRequest1 = Factory.createRentRequest(20.0, 2020, 10, Status.Accepted, itemRentLine1, rentee1);
        rentRequest2 = Factory.createRentRequest(20.0, 2020, 10, Status.Pending, itemRentLine1, rentee2);
        rentRequest3 = Factory.createRentRequest(5.0, 2020, 10, Status.Pending, itemRentLine3, rentee1);
        rentRequest4 = Factory.createRentRequest(5.0, 2020, 10, Status.Pending, itemRentLine3, rentee2);
        rentRequest5 = Factory.createRentRequest(3.0, 2020, 5, Status.Accepted, itemRentLine5, rentee1);
        rentRequest6 = Factory.createRentRequest(3.0, 2020, 5, Status.Accepted, itemRentLine5, rentee2);
//        rentRequest7 = TestFactory.createRentRequest(20.0, 2020, 10, Status.Rejected, itemRentLine1, rentee2);

        //raiting
        rating1 = Factory.createRating(5, 4, 4, rentRequest1, rentee1);
        rating2 = Factory.createRating(4, 4, 3, rentRequest2, rentee2);
        rating3 = Factory.createRating(3, 3, 3, rentRequest3, rentee1);
        rating4 = Factory.createRating(5, 4, 4, rentRequest4, rentee2);
        rating5 = Factory.createRating(5, 4, 5, rentRequest5, rentee1);
        rating6 = Factory.createRating(5, 5, 5, rentRequest6, rentee2);

        //question
        question1 = Factory.createQuestion("qs1", itemRentLine1, rentee1);
        question2 = Factory.createQuestion("qs2", itemRentLine1, rentee1);
        question3 = Factory.createQuestion("qs3", itemRentLine1, rentee1);
        question4 = Factory.createQuestion("qs4", itemRentLine2, rentee2);
        question5 = Factory.createQuestion("qs5", itemRentLine2, rentee2);
        question6 = Factory.createQuestion("qs6", itemRentLine2, rentee2);
        question7 = Factory.createQuestion("qs7", itemRentLine3, rentee1);
        question8 = Factory.createQuestion("qs8", itemRentLine3, rentee2);
        question9 = Factory.createQuestion("qs9", itemRentLine4, rentee2);
        question10 = Factory.createQuestion("qs10", itemRentLine5, rentee1);
        question11 = Factory.createQuestion("qs11", itemRentLine6, rentee2);

        //answer
        answer1 = Factory.createAnswer("ans1", question1, rentee1);
        answer2 = Factory.createAnswer("ans2", question1, rentee1);
        answer3 = Factory.createAnswer("ans3", question1, rentee1);
        answer4 = Factory.createAnswer("ans4", question2, rentee2);
        answer5 = Factory.createAnswer("ans5", question2, rentee2);
        answer6 = Factory.createAnswer("ans6", question2, rentee2);
        answer7 = Factory.createAnswer("ans7", question3, rentee1);
        answer8 = Factory.createAnswer("ans8", question3, rentee2);
        answer9 = Factory.createAnswer("ans9", question4, rentee1);
        answer10 = Factory.createAnswer("ans10", question7, rentee1);
        answer11 = Factory.createAnswer("ans11", question9, rentee1);

        items = Stream.of(item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12)
                .collect(Collectors.toList());

        users = Stream.of(
                admin, rentee1, rentee2, renter1, renter2
        ).collect(Collectors.toList());
    }
}
