import consoleFunctions.NonParallel;
import consoleFunctions.Parallel;
import factory.Factory;
import function.FunctionUtil;
import model.category.Category;
import model.category.SubCategory;
import model.item.Item;
import model.item.ItemRentLine;
import model.item.RentType;
import model.qna.Answer;
import model.qna.Question;
import model.rent_request.Rating;
import model.rent_request.RentRequest;
import model.user.Rentee;
import model.user.Renter;
import model.user.User;
import model.utils.Status;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConsoleApplication {
    private static Scanner scanner;



    public static void main(String[] args) {
        // initializing all the data


        scanner = new Scanner(System.in);

        printGreetings();


        System.out.println();

        System.out.print("Do you want to use parallel streaming on queries? (Y/N): ");
        String isParallel = scanner.nextLine();
        while((!(isParallel.charAt(0) == 'Y'|| isParallel.charAt(0) == 'N')) || isParallel.length() != 1){
            System.out.println("Please decide whether you should use Parallelism, based on your Objects' size");
            System.out.print("Enter Y or N:");
            isParallel = scanner.nextLine();
        }

        printChoices();

        if(isParallel.charAt(0) == 'Y'){
            ParallelTrack();
        }
        else if(isParallel.charAt(0) == 'N'){
            nonParallelTrack();
        }


    }

    static void nonParallelTrack(){
        System.out.print("Enter your choice (-1 to exit): ");
        int choice = scanner.nextInt();

        while (choice != -1) {
            NonParallel nonParallel = new NonParallel();
            switch (choice) {
                case 1 -> nonParallel.getTopKRatedItems();
                case 7 -> nonParallel.getTopKRentersMadeMostMoneyInAYear();
                case 13 -> nonParallel.getTopKItemsHavingMostRentRequestRejection();

                case 3 -> nonParallel.getTopKRenteesForYear();
                case 9 -> nonParallel.getTopKItemsHaveMostQuestions();
                case 15 -> nonParallel.getTopKItemsHavePendingRequests();

                case 5 -> nonParallel.getTopKRentedItems();
                case 11 -> nonParallel.getTopKusersAnsweredMostQuestions();
                case 17 -> nonParallel.getTopKItemsWithUnAnsweredQuestions();

                case 4 -> nonParallel.getTopKSubCategoriesHaveMostRentedItems();
                case 10 -> nonParallel.getTopKRentersAskedMostQuestions();
                case 16 -> nonParallel.getTopKItemsNotRecievedAnyRequest();

                case 2 -> nonParallel.getTopKRatedRenters();
                case 8 -> nonParallel.getTopKRenteesSpentMostMoneyInAYear();
                case 14 -> nonParallel.getTopKItemsHavingMostRentRequestAcceptance();
                default -> System.out.println("Invalid Choice");
            }

            System.out.print("Enter your choice (-1 to exit): ");
            choice = scanner.nextInt();
        }
    }

    static void ParallelTrack(){
        System.out.print("Enter your choice (-1 to exit): ");
        int choice = scanner.nextInt();

        while (choice != -1) {
            Parallel parallel = new Parallel();
            switch (choice) {
                case 1 -> parallel.getTopKRatedItems();
                case 7 -> parallel.getTopKRentersMadeMostMoneyInAYear();
                case 13 -> parallel.getTopKItemsHavingMostRentRequestRejection();

                case 3 -> parallel.getTopKRenteesForYear();
                case 9 -> parallel.getTopKItemsHaveMostQuestions();
                case 15 -> parallel.getTopKItemsHavePendingRequests();

                case 5 -> parallel.getTopKRentedItems();
                case 11 -> parallel.getTopKusersAnsweredMostQuestions();
                case 17 -> parallel.getTopKItemsWithUnAnsweredQuestions();

                case 4 -> parallel.getTopKSubCategoriesHaveMostRentedItems();
                case 10 -> parallel.getTopKRentersAskedMostQuestions();
                case 16 -> parallel.getTopKItemsNotRecievedAnyRequest();

                case 2 -> parallel.getTopKRatedRenters();
                case 8 -> parallel.getTopKRenteesSpentMostMoneyInAYear();
                case 14 -> parallel.getTopKItemsHavingMostRentRequestAcceptance();
                default -> System.out.println("Invalid Choice");
            }

            System.out.print("Enter your choice (-1 to exit): ");
            choice = scanner.nextInt();
        }
    }

    static void printGreetings() {
        System.out.println("=====================");
        System.out.println("Welcome to EasyRent");
        System.out.println("=====================");
    }


    static void printChoices() {
        System.out.println("|        1. Top Rated Items       |");
        System.out.println("|        2. Top Rated Renter       |");
        System.out.println("|        3. Top Rated Rentee       |");
        System.out.println("|        4. Top Sub Categories With Highest Rent Item Number       |");
        System.out.println("|        5. Top Rented Items       |");
        System.out.println("|        6. Items making most money       |");
        System.out.println("|        7. Top Renters Making Most Money       |");
        System.out.println("|        8. Top Rentee spending most money           |");
        System.out.println("|        9. Top Items with most questions           |");
        System.out.println("|        10.Top Renters asked most questions           |");
        System.out.println("|        11.Top user answered most answers for given items           |");
        System.out.println("|        12. Top Items with most rent requests           |");
        System.out.println("|        13. Items with most rejected requests           |");
        System.out.println("|        14. Items with most accepted requests           |");
        System.out.println("|        15. Items with most pending requests           |");
        System.out.println("|        16. Items without any requests           |");
        System.out.println("|        17. Items with most unanswered questions           |");
        System.out.println("|        18. Renter with most registered items           |");
        System.out.println("|        -1. Exit           |");
        System.out.println("============================");
    }



    static void printItems() {
        System.out.println("print");
    }

    static void printEmptyData() {
        System.out.println();
        System.out.println("NO DATA.. TRY AGAIN");
    }


}
