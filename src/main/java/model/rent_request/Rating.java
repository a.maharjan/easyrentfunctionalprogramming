package model.rent_request;

import model.user.User;
import model.utils.BaseEntity;

import java.time.LocalDateTime;

public class Rating extends BaseEntity {
    private int itemRating;
    private int renterRating;
    private int renteeRating;

    private RentRequest rentRequest;

    public Rating() {
    }

    public Rating(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                  LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                  boolean deleted, int itemRating, int renterRating, int renteeRating,
                  RentRequest rentRequest) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.itemRating = itemRating;
        this.renterRating = renterRating;
        this.renteeRating = renteeRating;
        this.rentRequest = rentRequest;
    }

    public int getItemRating() {
        return itemRating;
    }

    public int getRenterRating() {
        return renterRating;
    }

    public int getRenteeRating() {
        return renteeRating;
    }

    public RentRequest getRentRequest() {
        return rentRequest;
    }

    public void setItemRating(int itemRating) {
        this.itemRating = itemRating;
    }

    public void setRenterRating(int renterRating) {
        this.renterRating = renterRating;
    }

    public void setRenteeRating(int renteeRating) {
        this.renteeRating = renteeRating;
    }

    public void setRentRequest(RentRequest rentRequest) {
        this.rentRequest = rentRequest;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "itemRating=" + itemRating +
                ", renterRating=" + renterRating +
                ", renteeRating=" + renteeRating +
                ", rentRequest=" + rentRequest +
                '}';
    }
}
