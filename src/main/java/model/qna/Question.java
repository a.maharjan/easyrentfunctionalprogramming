package model.qna;

import model.item.ItemRentLine;
import model.user.User;
import model.utils.BaseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Question extends BaseEntity {
    private long id;
    private Question parent;
    private String question;

    private ItemRentLine itemRentLine;
    //Added by Sunil
    private List<Answer> answers = new ArrayList<>();

    public Question() {
    }

    public Question(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                    LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                    boolean deleted, long id, Question parent, String question,
                    ItemRentLine itemRentLine) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.id = id;
        this.parent = parent;
        this.question = question;
        this.itemRentLine = itemRentLine;
    }

    public long getId() {
        return id;
    }

    public Question getParent() {
        return parent;
    }

    public String getQuestion() {
        return question;
    }

    public ItemRentLine getItemRentLine() {
        return itemRentLine;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setParent(Question parent) {
        this.parent = parent;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setItemRentLine(ItemRentLine itemRentLine) {
        this.itemRentLine = itemRentLine;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Question{" +
                "parent=" + parent +
                ", question='" + question + '\'' +
                ", itemRentLine=" + itemRentLine +
                '}';
    }
}
