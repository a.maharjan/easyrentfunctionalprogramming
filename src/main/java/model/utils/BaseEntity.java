package model.utils;

import model.user.User;

import java.time.LocalDateTime;

public class BaseEntity {
    private User createdBy;
    private LocalDateTime creationDate;

    private User lastModifiedBy;
    private LocalDateTime lastModificationDate;

    private User deletedBy;
    private LocalDateTime deletionDate;

    private boolean deleted = false;

    public BaseEntity() {
    }

    public BaseEntity(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                      LocalDateTime lastModificationDate, User deletedBy,
                      LocalDateTime deletionDate, boolean deleted) {
        this.createdBy = createdBy;
        this.creationDate = creationDate;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModificationDate = lastModificationDate;
        this.deletedBy = deletedBy;
        this.deletionDate = deletionDate;
        this.deleted = deleted;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(User lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateTime getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDateTime lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public User getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(User deletedBy) {
        this.deletedBy = deletedBy;
    }

    public LocalDateTime getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(LocalDateTime deletionDate) {
        this.deletionDate = deletionDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
