package model.utils;

public enum Status {
    Pending,
    Accepted,
    Rejected
}
