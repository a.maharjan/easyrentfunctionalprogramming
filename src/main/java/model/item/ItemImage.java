package model.item;

import model.user.User;
import model.utils.BaseEntity;

import java.time.LocalDateTime;

public class ItemImage extends BaseEntity {
    private long id;
    private String imagePath;
    private boolean mainImage;
    private Item item;

    public ItemImage() {
    }

    public ItemImage(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                     LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                     boolean deleted, long id, String imagePath, boolean mainImage, Item item) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy, deletionDate, deleted);
        this.id = id;
        this.imagePath = imagePath;
        this.mainImage = mainImage;
        this.item = item;
    }

    public long getId() {
        return id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public boolean isMainImage() {
        return mainImage;
    }

    public Item getItem() {
        return item;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setMainImage(boolean mainImage) {
        this.mainImage = mainImage;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
