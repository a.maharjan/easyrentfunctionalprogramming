package model.item;

public enum RentType {
    HOURLY,
    DAILY,
    MONTHLY,
    YEARLY
}
