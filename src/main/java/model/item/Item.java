package model.item;

import model.category.SubCategory;
import model.user.User;
import model.utils.BaseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Item extends BaseEntity {
    private long id;
    private String name;
    private String description;

    private List<ItemRentLine> itemRentLines = new ArrayList<>();
    private SubCategory subCategory = null;
    private List<ItemImage> itemImages = new ArrayList<>();

    public Item() {
    }

    public Item(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                boolean deleted, long id, String name, String description,
                List<ItemRentLine> itemRentLines, SubCategory subCategory, List<ItemImage> itemImages) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.id = id;
        this.name = name;
        this.description = description;
        this.itemRentLines = itemRentLines;
        this.subCategory = subCategory;
        this.itemImages = itemImages;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<ItemRentLine> getItemRentLines() {
        return itemRentLines;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public List<ItemImage> getItemImages() {
        return itemImages;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setItemRentLines(List<ItemRentLine> itemRentLines) {
        this.itemRentLines = itemRentLines;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public void setItemImages(List<ItemImage> itemImages) {
        this.itemImages = itemImages;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", subCategory=" + subCategory +
                '}';
    }
}
