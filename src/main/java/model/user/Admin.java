package model.user;

import model.category.Category;
import model.category.CategoryRequest;

import java.util.List;

public class Admin extends Role {
    private List<Category> categories;
    private List<CategoryRequest> categoryRequests;

    public Admin() {
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<CategoryRequest> getCategoryRequests() {
        return categoryRequests;
    }

    public void setCategoryRequests(List<CategoryRequest> categoryRequests) {
        this.categoryRequests = categoryRequests;
    }
}
