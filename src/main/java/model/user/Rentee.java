package model.user;

import model.rent_request.RentRequest;

import java.util.HashSet;
import java.util.Set;

public class Rentee extends Role {

    public Rentee() {
    }

    private Set<RentRequest> rentRequests = new HashSet<>();

    public Rentee(Set<RentRequest> rentRequests) {
        this.rentRequests = rentRequests;
    }

    public Set<RentRequest> getRentRequests() {
        return rentRequests;
    }

    public void setRentRequests(Set<RentRequest> rentRequests) {
        this.rentRequests = rentRequests;
    }
}
