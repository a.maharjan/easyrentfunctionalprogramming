package model.user;

public abstract class Role {
	private int id;
	private ERole name;

	private User user;

	public Role() {
	}

	public Integer getId() {
		return id;
	}

	public ERole getName() {
		return name;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(ERole name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Role{" +
				"id=" + id +
				", name=" + name +
				'}';
	}
}