package model.user;

import model.item.ItemRentLine;

import java.util.HashSet;
import java.util.Set;

public class Renter extends Role {
    private Set<ItemRentLine> rentLines = new HashSet<>();

    public Renter() {
    }

    public Set<ItemRentLine> getRentLines() {
        return rentLines;
    }

    public void setRentLines(Set<ItemRentLine> rentLines) {
        this.rentLines = rentLines;
    }
}
