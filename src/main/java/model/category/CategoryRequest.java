package model.category;

import model.user.User;
import model.utils.BaseEntity;
import model.utils.Status;

import java.time.LocalDateTime;

public class CategoryRequest extends BaseEntity {
    private long id;
    private String name;
    private String description;
    private String imagePath;
    private Status status;
    private  String comment;

    public CategoryRequest() {
    }

    public CategoryRequest(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                           LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                           boolean deleted, long id, String name, String description,
                           String imagePath, Status status, String comment) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.id = id;
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.status = status;
        this.comment = comment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
