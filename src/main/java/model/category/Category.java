package model.category;

import model.user.User;
import model.utils.BaseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Category extends BaseEntity {
    private long id;
    private String name;
    private String description;
    private String imagePath;

    private List<SubCategory> subCategories = new ArrayList<>();

    public Category() {
    }

    public Category(User createdBy, LocalDateTime creationDate, User lastModifiedBy,
                    LocalDateTime lastModificationDate, User deletedBy, LocalDateTime deletionDate,
                    boolean deleted, long id, String name, String description, String imagePath, List<SubCategory> subCategories) {
        super(createdBy, creationDate, lastModifiedBy, lastModificationDate, deletedBy,
                deletionDate, deleted);
        this.id = id;
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.subCategories = subCategories;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }
}
