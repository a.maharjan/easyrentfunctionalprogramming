package function.utils;

//todo: Make this interface a valid definition for a function that accepts 4 inputs and return a single value
@FunctionalInterface
public interface QuadFunction<S, T, U, V, R>{
    R apply(S s, T t, U u, V v);
}
