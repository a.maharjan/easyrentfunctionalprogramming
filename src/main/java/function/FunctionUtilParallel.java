package function;

import dto.Tuple;
import dto.item.ItemRating;
import dto.rent_request.ItemAcceptanceCount;
import dto.rent_request.ItemRejectionCount;
import dto.user.TupleRenterRating;
import dto.user.TupleSpentAmountOfRentee;
import dto.user.UserEarning;
import function.utils.TriFunction;
import model.item.Item;
import model.rent_request.Rating;
import model.rent_request.RentRequest;
import model.user.Rentee;
import model.user.Renter;
import model.user.User;
import model.utils.Status;

import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface FunctionUtilParallel {



    Function<List<User>, List<Rentee>> usersToRentees = users ->
            users.parallelStream()
                    .flatMap(p -> p.getRoles().parallelStream())
                    .filter(r -> r instanceof Rentee)
                    .map(r -> (Rentee) r)
                    .collect(Collectors.toList());

    Function<List<User>, List<Renter>> usersToRenters = users ->
            users.parallelStream()
                    .flatMap(p -> p.getRoles().parallelStream())
                    .filter(r -> r instanceof Renter)
                    .map(r -> (Renter) r)
                    .collect(Collectors.toList());

    // 1. ------- Calculate topKRatedItems (by Anish Maharjan) --------
    Function<Item, Long> calculateRating = (item) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().parallelStream())
                    .map(RentRequest::getRating)
                    .mapToLong(Rating::getItemRating)
                    .sum();

    BiFunction<List<Item>, Integer, List<Item>> topKRatedItems = (items, k) ->
            items.parallelStream()
                    // ItemRating is a custom class to hold (Item, totalRating) tuple.
                    .map(item -> new ItemRating(item, calculateRating.apply(item)))
                    .sorted(Comparator.comparing(ItemRating::getSumOfRating).reversed()) // descending order
                    .limit(k)
                    .map(ItemRating::getItem)
                    .collect(Collectors.toList());
    // ------------------ End of Qsn 1 -----------------

    // 2. ---- Calculate topKRatedRenter (by Komiljon Aliev) -----
    BiFunction<Renter, Integer, Double> getAveragaRatingOfRenter = (renter, year) ->
            Stream.of(renter)
                    .flatMap(u -> u.getRentLines().parallelStream())
                    .flatMap(rentLine -> rentLine.getRentRequests().parallelStream())
                    .filter(r -> r.getStatus() == Status.Accepted
                            && r.getCreationDate().getYear() == year)
                    .mapToInt(r -> r.getRating().getRenterRating())
                    .average().orElse(0.0d);



    TriFunction<List<User>, Integer, Integer, List<Renter>> topKRatedRenter = (users, year, k) ->
            users.parallelStream()
                    .flatMap(u -> u.getRoles().parallelStream())
                    .filter( r -> r instanceof Renter)
                    .map( r -> (Renter)r)
                    .map(u -> new TupleRenterRating(u, getAveragaRatingOfRenter.apply(u, year)))
                    .sorted(Comparator.comparing(TupleRenterRating::getRating).reversed())
                    .limit(k)
                    .map(t -> t.getRenter())
                    .collect(Collectors.toList());
    // ------------------ End of Qsn 2 -----------------

    //Query #3 - Return emails for top k rentees rented most items for a specific year
    TriFunction<List<User>, Integer, Integer, List<String>> topKRenteesForYear  = (users, year, k) ->
            usersToRentees.apply(users).parallelStream()
                    .flatMap(u -> u.getRentRequests().parallelStream())
                    .filter(r -> r.getStatus() == Status.Accepted)
                    .filter(r -> r.getFromDate().getYear() == year)
                    .collect(Collectors.groupingBy(r -> r.getCreatedBy(), Collectors.counting()))
                    .entrySet().parallelStream()
                    .sorted((r1, r2) -> (int)(r2.getValue() - r1.getValue()))
                    .limit(k)
                    .map(r -> r.getKey().getEmail())
                    .collect(Collectors.toList());
    //End-Query #3 - Return emails for top k rentee rented most items for a specific year


    // 4 . top K SubCategories Have Most Rented Items (by Abdessamad Jadid)

    TriFunction<List<Item>, Integer, Integer, List<String>> topKSubCategoriesHaveMostRentedItems = (items, year, k) ->
            items.parallelStream()
                    .flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(f ->f.getRentRequests().parallelStream())
                    .filter(fi ->fi.getStatus() == Status.Accepted)
                    .collect(Collectors.groupingBy(r->r.getItemRentLine().getItem().getSubCategory(),
                            Collectors.counting()
                            ))
                    .entrySet().parallelStream()
                    .sorted((a, b) -> (int)(b.getValue() - a.getValue()))
                    .limit(k)
                    .map(t -> t.getKey().getName())
                    .collect(Collectors.toList());

    //End 4

    //5. ------- TopKRentedItems----------
    BiFunction<List<Item>, Integer, List<Item>> getTopKRentedItems = (items, k) ->
            items.parallelStream().flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(rl -> rl.getRentRequests().parallelStream())
                    .filter(rr -> rr.getStatus() == Status.Accepted)
                    .collect(Collectors.groupingBy(rr -> rr.getItemRentLine().getItem(), Collectors.counting()))
                    .entrySet().parallelStream().sorted((g1, g2) -> (int) (g2.getValue() - g1.getValue()))
                    .limit(k)
                    .map(g -> g.getKey()).distinct().collect(Collectors.toList());
    //5. ------- TopKRentedItems----------


    // 7. ---- Calculate topKRentersMadeMostMoneyInAYear (by Anish Maharjan) -----
    BiFunction<Item, Integer, Double> calculateEarningsOfAnItem = (item, year) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().parallelStream())
                    // filtering RentRequest by year
                    .filter(rentRequest -> rentRequest.getFromDate().getYear() == year ||
                            rentRequest.getToDate().getYear() == year)
                    .mapToDouble(r -> r.getPrice() * ChronoUnit.DAYS.between(r.getFromDate(), r.getToDate())) // get only price from RentRequest
                    .sum(); // sum of all the price of a particular item

    TriFunction<List<Item>, Integer, Integer, List<User>> topKRentersMadeMostMoneyInAYear =
            (items, k, year) ->
                    items.parallelStream()
                            // UserEarning is a custom class to hold (User, totalEarning) tuple.
                            .map(item -> new UserEarning(item.getCreatedBy(),
                                    calculateEarningsOfAnItem.apply(item, year)))
                            .filter(userEarning -> userEarning.getTotalEarning()>0)
                            .sorted(Comparator.comparing(UserEarning::getTotalEarning, Comparator.reverseOrder()))
                            .map(UserEarning::getUser)
                            .distinct()
                            .limit(k)
                            .collect(Collectors.toList());

    // ------------------ End of Qsn 7 -----------------

    // 8. ---- Calculate topKRenteesSpentMostMoneyInAYear (by Komiljon Aliev) -----
    BiFunction<Rentee, Integer, Double> getSpentAmountOfRentee = (rentee, year) ->
            Stream.of(rentee)
                    .flatMap(rente -> rente.getRentRequests().parallelStream())
                    .filter(rr -> rr.getStatus() == Status.Accepted
                            && rr.getCreationDate().getYear() == year)
                    .mapToDouble(r -> r.getPrice() * ChronoUnit.DAYS.between(r.getFromDate(), r.getToDate()))
                    .sum()
            ;

    TriFunction<List<User>, Integer, Integer, List<Rentee>> topKRenteesSpentMostMoneyInAYear = (rentees, year, k) ->
            rentees.parallelStream()
                    .flatMap(rentee -> rentee.getRoles().parallelStream())
                    .filter(r -> r instanceof  Rentee)
                    .map(r -> (Rentee)r)
                    .map(u -> new TupleSpentAmountOfRentee(u, getSpentAmountOfRentee.apply(u, year)))
                    .sorted(Comparator.comparing(TupleSpentAmountOfRentee::getSpentAmount).reversed())
                    .limit(k)
                    .map(t -> t.getRentee())
                    .collect(Collectors.toList());

    // ------------------ End of Qsn 8 -----------------

    //Query #9 - Return names for top k items has most question in their rent-line for a specific year
    TriFunction<List<User>, Integer, Integer, List<String>> topKItemsHaveMostQuestions  = (users, year, k) ->
            usersToRenters.apply(users).parallelStream()
                    .flatMap(u -> u.getRentLines().parallelStream())
                    .filter(r -> r.getFromDate().getYear() == year)
                    .flatMap(r -> r.getQuestions().parallelStream())
                    //.filter(r -> r.getCreationDate().getYear() == year)
                    .collect(Collectors.groupingBy(r -> r.getItemRentLine().getItem(), Collectors.counting()))
                    .entrySet().parallelStream()
                    .sorted((r1, r2) -> (int)(r2.getValue() - r1.getValue()))
                    .limit(k)
                    .map(r -> r.getKey().getName())
                    .collect(Collectors.toList());
    //End-Query #9 - Return names for top k items has most question in their rent-line for a specific year


    // 10 - top K Renters' emails Been Asked Most Questions (by Abdessamad Jadid)
    TriFunction<List<User>, Integer, Integer, List<String>> topKRentersAskedMostQuestions  = (users, year, k) ->
            usersToRenters.apply(users).parallelStream()
                    .flatMap(u -> u.getRentLines().parallelStream())
                    .filter(r -> r.getFromDate().getYear() == year)
                    .flatMap(r -> r.getQuestions().parallelStream())
                    //.filter(r -> r.getCreationDate().getYear() == year)
                    .collect(Collectors.groupingBy(r -> r.getItemRentLine().getCreatedBy(), Collectors.counting()))
                    .entrySet().parallelStream()
                    .sorted((r1, r2) -> (int)(r2.getValue() - r1.getValue()))
                    .limit(k)
                    .map(r -> r.getKey().getEmail())
                    .collect(Collectors.toList());
    // End 10

    //    11. Top K users who answered most questions in certain year
    TriFunction<List<Item>, Integer, Integer, List<User>> getTopKusersAnsweredMostQuestions = (items, year, k) ->
            items.parallelStream().flatMap(u -> u.getItemRentLines().parallelStream())
                    .filter(rl -> rl.getCreationDate().getYear() == year)
                    .flatMap(rl -> rl.getQuestions().parallelStream())
                    .flatMap(q -> q.getAnswers().parallelStream())
                    .collect(Collectors.groupingBy(x -> x.getCreatedBy(), Collectors.counting()))
                    .entrySet().parallelStream()
                    .sorted((g1, g2) -> (int) (g2.getValue() - g1.getValue()))
                    .limit(k)
                    .map(g -> g.getKey())
                    .collect(Collectors.toList());
    //    11. Top K users who answered most questions in certain year


    // 13. ------- Calculate topKItemsHavingMostRentRequestRejection (by Anish Maharjan) --------
    Function<Item, Long> countTheRejectionStatusOfRentRequestOfAnItem = (item) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().parallelStream())
                    .filter(rentRequest -> rentRequest.getStatus() == Status.Rejected)
                    .count();

    BiFunction<List<Item>, Integer, List<Item>> topKItemsHavingMostRentRequestRejection =
            (items, k) ->
                    items.parallelStream()
                            .map(item -> new ItemRejectionCount(item,
                                    countTheRejectionStatusOfRentRequestOfAnItem.apply(item)))
                            .filter(count -> count.getRejectionCount() > 0)
                            .sorted(Comparator.comparing(ItemRejectionCount::getRejectionCount).reversed())
                            .limit(k)
                            .map(ItemRejectionCount::getItem)
                            .collect(Collectors.toList());

    // ------------------ End of Qsn 13 -----------------

    // 14. ------- Calculate topKItemsHavingMostRentRequestAcceptance (by Komiljon Aliev) --------
    Function<Item, Long> countTheAcceptanceStatusOfRentRequestOfAnItem = (item) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().parallelStream())
                    .filter(rentRequest -> rentRequest.getStatus() == Status.Accepted)
                    .count();

    BiFunction<List<Item>, Integer, List<Item>> topKItemsHavingMostRentRequestAcceptance =
            (items, k) ->
                    items.parallelStream()
                            .map(item -> new ItemAcceptanceCount(item,
                                    countTheAcceptanceStatusOfRentRequestOfAnItem.apply(item)))
                            .sorted(Comparator.comparing(ItemAcceptanceCount::getAcceptanceCount).reversed())
                            .limit(k)
                            .map(ItemAcceptanceCount::getItem)
                            .collect(Collectors.toList());

    // ------------------ End of Qsn 14 -----------------


    //Query #15 - Return names for top k items has most pending rent-request in their rent-line for a specific year
    TriFunction<List<User>, Integer, Integer, List<String>> topKItemsHavePendingRequests  = (users, year, k) ->
            usersToRentees.apply(users).parallelStream()
                    .flatMap(u -> u.getRentRequests().parallelStream())
                    .filter(r -> r.getFromDate().getYear() == year
                            && r.getStatus() == Status.Pending)
                    .collect(Collectors.groupingBy(r -> r.getItemRentLine().getItem(), Collectors.counting()))
                    .entrySet().parallelStream()
                    .sorted((r1, r2) -> (int)(r2.getValue() - r1.getValue()))
                    .limit(k)
                    .map(r -> r.getKey().getName())
                    .collect(Collectors.toList());
    //End-Query #15 - Return names for top k items has most pending rent-request in their rent-line for a specific year

    // 16. top K Items Not Recieved Any Request (by Abdessamad Jadid)

    BiFunction<Item, Integer, Long> countTotalRentRequests = (item, year) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().parallelStream())
                    .filter(i -> i.getFromDate().getYear() == year && i.getToDate().getYear() == year)

                    .count();

    TriFunction<List<Item>, Integer, Integer, List<Item>> topKItemsNotRecievedAnyRequest = (items, year, k) ->
            items.parallelStream()
                    .map(i -> new Tuple<Item, Long>(i, countTotalRentRequests.apply(i, year)))
                    .filter(i ->i.getValue() == 0)
                    .sorted((a, b) -> (int)(a.getValue() - b.getValue()))
                    .limit(k)
                    .map(t -> t.getKey())
                    .collect(Collectors.toList());
    //End  16


    //17 Top K items with highest number of unanswered questions
    BiFunction<List<Item>, Integer, List<Item>> getTopKItemsWithUnAnsweredQuestions = (items, k)
            -> items.parallelStream()
            .flatMap(i -> i.getItemRentLines().parallelStream())
            .flatMap(rl -> rl.getQuestions().parallelStream())
            .filter(q -> q.getAnswers().parallelStream().count() == 0)
            .collect(Collectors.groupingBy(q -> q.getItemRentLine().getItem(), Collectors.counting()))
            .entrySet().parallelStream()
//            .collect(Collectors.groupingBy(g -> g.getKey().getItem(), Collectors.summingLong(gw -> gw.getValue())))
//            .entrySet().parallelStream()
//
            .sorted((g1, g2) -> (int) (g2.getValue() - g1.getValue()))
            .limit(k)
            .map(x -> x.getKey()).collect(Collectors.toList());
    //17 Top K items with highest number of unanswered questions

    // 6  topKItemWhoMadeMostMoney
    BiFunction<Item, Integer, Double> calculateSumRentPrice = (item, year) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().parallelStream())
                    .filter(i -> i.getFromDate().getYear() == year && i.getToDate().getYear() == year && i.getStatus() == Status.Accepted)
                    .mapToDouble(RentRequest::getPrice)
                    .sum();


    TriFunction<List<Item>, Integer, Integer, List<Item>> topKItemWhoMadeMostMoney = (items, year, k) ->
            items.parallelStream()
                    .map(i -> new Tuple<Item, Double>(i, calculateSumRentPrice.apply(i, year)))
                    .sorted((a, b) -> Double.compare(b.getValue(), a.getValue()))
                    .limit(k)
                    .map(t -> t.getKey())
                    .collect(Collectors.toList());


    // 12 topKItemsHaveMostRentRequest
    BiFunction<Item, Integer, Long> countTotalRentRequest = (item, year) ->
            Stream.of(item)
                    .flatMap(i -> i.getItemRentLines().parallelStream())
                    .flatMap(itemRentLine -> itemRentLine.getRentRequests().parallelStream())
                    .filter(i -> i.getFromDate().getYear() == year && i.getToDate().getYear() == year)
                    .count();

    TriFunction<List<Item>, Integer, Integer, List<Item>> topKItemsHaveMostRentRequest = (items, year, k) ->
            items.parallelStream()
                    .map(i -> new Tuple<Item, Long>(i, countTotalRentRequest.apply(i, year)))
                    .sorted((a, b) -> Long.compare(b.getValue(), a.getValue()))
                    .limit(k)
                    .map(t -> t.getKey())
                    .collect(Collectors.toList());


    // 18 topKRentersHaveRegisteredItem
    public static TriFunction<List<User>, Integer, Integer, String> topKRentersHaveRegisteredItem = (users, year, k) ->

            usersToRenters.apply(users).parallelStream()
                    .flatMap(renter -> renter.getRentLines().parallelStream())
                    .filter(rl -> rl.getFromDate().getYear() == year)
                    .collect(Collectors.groupingBy(r -> r.getCreatedBy(), Collectors.counting()))
                    .entrySet().parallelStream()
                    .sorted((a, b) -> Long.compare(b.getValue(), a.getValue()))
                    .map(result -> result.getKey())
                    .limit(k)
                    .map(u -> u.getEmail())
                    .collect(Collectors.joining(","));


}
