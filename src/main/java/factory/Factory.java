package factory;

import model.category.Category;
import model.category.SubCategory;
import model.item.Item;
import model.item.ItemRentLine;
import model.item.RentType;
import model.qna.Answer;
import model.qna.Question;
import model.rent_request.Rating;
import model.rent_request.RentRequest;
import model.user.*;
import model.utils.Status;

import java.time.LocalDateTime;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Factory {
    public static User createAdmin(String username, String email){
        User user = new User();
        user.setEmail(email);
        user.setUsername(username);

        Admin role = new Admin();
        role.setName(ERole.ROLE_ADMIN);
        role.setUser(user);

        user.setRoles(Stream.of(role).collect(Collectors.toSet()));
        return user;
    }

    public static User createRentee(String username, String email){
        User user = new User();
        user.setEmail(email);
        user.setUsername(username);

        Rentee role = new Rentee();
        role.setName(ERole.ROLE_USER);
        role.setUser(user);

        user.setRoles(Stream.of(role).collect(Collectors.toSet()));
        return user;
    }

    public static User createRenter(String username, String email){
        User user = new User();
        user.setEmail(email);
        user.setUsername(username);

        Renter role = new Renter();
        role.setName(ERole.ROLE_USER);
        role.setUser(user);

        user.setRoles(Stream.of(role).collect(Collectors.toSet()));
        return user;
    }


    public static Category createCategory(String name, String description, User user){
        Category category = new Category();
        category.setName(name);
        category.setDescription(description);
        category.setCreationDate(LocalDateTime.now());
        category.setCreatedBy(user);
        return category;
    }

    public static SubCategory createSubCategory(String name, String description,
                                                User user, Category category){
        SubCategory subCategory = new SubCategory();
        subCategory.setName(name);
        subCategory.setDescription(description);
        subCategory.setCreatedBy(user);
        subCategory.setCreationDate(LocalDateTime.now());
        subCategory.setCategory(category);

        category.getSubCategories().add(subCategory);

        return subCategory;
    }

    public static Item createItem(String name, String description, User user, SubCategory subCategory){
        Item item = new Item();
        item.setName(name);
        item.setDescription(description);
        item.setSubCategory(subCategory);

        item.setCreatedBy(user);
        item.setCreationDate(LocalDateTime.now());

        subCategory.getItems().add(item);

        return item;
    }

    public static ItemRentLine createItemRentLine(double price, int year, int days, RentType rentType,
                                                  Item item, User user){
        ItemRentLine itemRentLine = new ItemRentLine();
        itemRentLine.setPrice(price);

        LocalDateTime basetime=LocalDateTime.now().withYear(year);
        itemRentLine.setFromDate(basetime);
        itemRentLine.setToDate(basetime.plusDays(days));

        itemRentLine.setRentType(rentType);
        itemRentLine.setItem(item);

        itemRentLine.setCreatedBy(user);
        itemRentLine.setCreationDate(basetime);

        item.getItemRentLines().add(itemRentLine);

        user.getRoles().stream()
                .filter(r -> r instanceof Renter)
                .map(r -> (Renter) r)
                .findFirst().get()
                .getRentLines().add(itemRentLine);

        return itemRentLine;
    }

    public static Question createQuestion(String text, ItemRentLine itemRentLine, User user){
        Question question = new Question();
        question.setQuestion(text);
        question.setItemRentLine(itemRentLine);

        question.setCreatedBy(user);
        question.setCreationDate(LocalDateTime.now());

        itemRentLine.getQuestions().add(question);

        return question;
    }

    public static Answer createAnswer(String text, Question question, User user){
        Answer answer = new Answer();
        answer.setAnswer(text);
        answer.setQuestion(question);

        answer.setCreatedBy(user);
        answer.setCreationDate(LocalDateTime.now());

        question.getAnswers().add(answer);

        return answer;
    }

    public static Rating createRating(int itemRating, int renterRating, int renteeRating,
                                      RentRequest rentRequest, User user){
        Rating rating = new Rating();
        rating.setItemRating(itemRating);
        rating.setRenteeRating(renteeRating);
        rating.setRenterRating(renterRating);
        rating.setRentRequest(rentRequest);

        rating.setCreatedBy(user);
        rating.setCreationDate(LocalDateTime.now());

        rentRequest.setRating(rating);

        return rating;
    }

    public static RentRequest createRentRequest(double price, int year, int days, Status status,
                                                ItemRentLine itemRentLine, User user){
        RentRequest rentRequest = new RentRequest();
        rentRequest.setPrice(price);

        LocalDateTime basetime=LocalDateTime.now().withYear(year);
        rentRequest.setFromDate(basetime);
        rentRequest.setToDate(basetime.plusDays(days));

        rentRequest.setStatus(status);
        rentRequest.setItemRentLine(itemRentLine);

        rentRequest.setCreatedBy(user);
        rentRequest.setCreationDate(LocalDateTime.now());

        itemRentLine.getRentRequests().add(rentRequest);

        user.getRoles().stream()
                .filter(r -> r instanceof Rentee)
                .map(r -> (Rentee) r)
                .findFirst().get()
                .getRentRequests().add(rentRequest);

        return rentRequest;
    }
}
