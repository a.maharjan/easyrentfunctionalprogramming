package dto.user;

import model.user.User;

public class UserEarning {
    private User user;
    private double totalEarning;

    public UserEarning(User user, double totalEarning) {
        this.user = user;
        this.totalEarning = totalEarning;
    }

    public User getUser() {
        return user;
    }

    public double getTotalEarning() {
        return totalEarning;
    }
}
