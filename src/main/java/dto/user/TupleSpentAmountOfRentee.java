package dto.user;

import model.user.Rentee;

public class TupleSpentAmountOfRentee {
    private Rentee rentee;
    private Double spentAmount;

    public TupleSpentAmountOfRentee(Rentee rentee, Double spentAmount) {
        this.rentee = rentee;
        this.spentAmount = spentAmount;
    }

    public Rentee getRentee() {
        return rentee;
    }

    public void setRentee(Rentee rentee) {
        this.rentee = rentee;
    }

    public Double getSpentAmount() {
        return spentAmount;
    }

    public void setSpentAmount(Double spentAmount) {
        this.spentAmount = spentAmount;
    }
}
