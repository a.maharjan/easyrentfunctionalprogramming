package dto.user;

import model.user.Renter;

public class TupleRenterRating {
    private Renter renter;
    private double rating;

    public TupleRenterRating(Renter renter, double rating) {
        this.renter = renter;
        this.rating = rating;
    }

    public Renter getRenter() {
        return renter;
    }

    public void setRenter(Renter renter) {
        this.renter = renter;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
