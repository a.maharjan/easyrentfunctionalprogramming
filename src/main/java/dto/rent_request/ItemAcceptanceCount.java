package dto.rent_request;

import model.item.Item;

public class ItemAcceptanceCount {
    private Item item;
    private long acceptanceCount; // count of the accepted rent requestion

    public ItemAcceptanceCount(Item item, long acceptanceCount) {
        this.item = item;
        this.acceptanceCount = acceptanceCount;
    }

    public Item getItem() {
        return item;
    }

    public long getAcceptanceCount() {
        return acceptanceCount;
    }
}
