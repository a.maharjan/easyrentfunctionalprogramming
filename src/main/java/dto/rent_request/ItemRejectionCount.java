package dto.rent_request;

import model.item.Item;

public class ItemRejectionCount {
    private Item item;
    private long rejectionCount; // count of the rejected rent requestion

    public ItemRejectionCount(Item item, long rejectionCount) {
        this.item = item;
        this.rejectionCount = rejectionCount;
    }

    public Item getItem() {
        return item;
    }

    public long getRejectionCount() {
        return rejectionCount;
    }
}
