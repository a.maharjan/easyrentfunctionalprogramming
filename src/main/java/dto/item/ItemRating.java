package dto.item;

import model.item.Item;

public class ItemRating {
    private Item item;
    private Long sumOfRating;

    public ItemRating(Item item, Long sumOfRating) {
        this.item = item;
        this.sumOfRating = sumOfRating;
    }

    public Item getItem() {
        return item;
    }

    public Long getSumOfRating() {
        return sumOfRating;
    }
}
